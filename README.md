# MPermissions
本项目是基于开源项目MPermissions进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/hongyangAndroid/MPermissions).


移植版本：源master版本

## 项目介绍
### 项目名称：mpermissions
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    基于Annotation Processor的简单易用的处理harmonyos运行时权限的库


### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/hongyangAndroid/MPermissions
### 编程语言：java

## 安装教程

#### 方案一

建议下载开源代码并参照demo引入相关库：
```
 dependencies {
    implementation project(':permission-annotation')
    implementation project(path: ':permission-lib')
    annotationProcessor 'com.zhy:mpermission-compiler:1.0.0'
 }  
```

#### 方案二

* 项目根目录的build.gradle中的repositories添加：
```groovy
buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```

* module目录的build.gralde中dependencies添加：
```groovy
 dependencies {
     implementation 'com.gitee.ts_ohos:permission-lib:1.0.0'
     implementation 'com.gitee.ts_ohos:permission-annotation:1.0.0'
     annotationProcessor 'com.zhy:mpermission-compiler:1.0.0'
 }
```

## 使用说明

* 申请权限

```java
MPermissions.requestPermissions(MainAbilitySlice.this, REQUECT_CODE_SDCARD, "ohos.permission.WRITE_MEDIA");
```

* 处理权限回调

```java
@Override
public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
   MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
   super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
}
```

* 是否需要弹出解释

```
if (!MPermissions.shouldShowRequestPermissionRationale(MainAbilitySlice.this, "ohos.permission.WRITE_MEDIA", REQUECT_CODE_SDCARD)) {
   MPermissions.requestPermissions(MainAbilitySlice.this, REQUECT_CODE_SDCARD, "ohos.permission.WRITE_MEDIA");
}
```

如果需要解释，会自动执行使用`@ShowRequestPermissionRationale`注解的方法。

授权成功以及失败调用的分支方法通过注解`@PermissionGrant`和`@PermissionDenied`进行标识，详细参考下面的例子或者entry。

## 例子

* in MainAbilitySlice:

```java
public class MainAbilitySlice extends AbilitySlice {

    private Button mBtnSdcard, mBtnReadHealthData, mBtnMicrophone;
    
        @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            super.setUIContent(ResourceTable.Layout_ability_main);
            mBtnSdcard = (Button) findComponentById(ResourceTable.Id_id_btn_sdcard);
            mBtnReadHealthData = (Button) findComponentById(ResourceTable.Id_id_btn_readHealthData);
            mBtnMicrophone = (Button) findComponentById(ResourceTable.Id_id_btn_microphone);
    
            mBtnSdcard.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (!MPermissions.shouldShowRequestPermissionRationale(
                            MainAbilitySlice.this,
                            "ohos.permission.WRITE_MEDIA", REQUECT_CODE_SDCARD)) {
                        MPermissions.requestPermissions(
                                MainAbilitySlice.this,
                                REQUECT_CODE_SDCARD,
                                "ohos.permission.WRITE_MEDIA");
                    }
                }
            });
    
            mBtnReadHealthData.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    MPermissions.requestPermissions(MainAbilitySlice.this,
                            REQUECT_CODE_READ_HEALTH_DATA, "ohos.permission.READ_HEALTH_DATA");
                }
            });
    
            mBtnMicrophone.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    MPermissions.requestPermissions(
                            MainAbilitySlice.this, REQUECT_CODE_MICROPHONE, "ohos.permission.MICROPHONE");
                }
            });
        }
    
        @ShowRequestPermissionRationale(REQUECT_CODE_SDCARD)
        public void whyNeedScCard() {
            new ToastDialog(this).setText("I need write news to sdcard!").setDuration(0).show();
            MPermissions.requestPermissions(
                    MainAbilitySlice.this, REQUECT_CODE_SDCARD, "ohos.permission.WRITE_MEDIA");
        }
    
        @Override
        public void onActive() {
            super.onActive();
        }
    
        @Override
        public void onForeground(Intent intent) {
            super.onForeground(intent);
        }
}
```

* in MainAbility:

```java

public class MainAbility extends Ability {

    @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            super.setMainRoute(MainAbilitySlice.class.getName());
        }
    
        @Override
        public void onRequestPermissionsFromUserResult(int requestCode,
                                                       String[] permissions,
                                                       int[] grantResults) {
            MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
            super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        }
    
        @ShowRequestPermissionRationale(REQUECT_CODE_SDCARD)
        public void whyNeedScCard() {
            new ToastDialog(this).setText("I need write news to sdcard!").setDuration(0).show();
            MPermissions.requestPermissions(
                    MainAbility.this,
                    REQUECT_CODE_SDCARD,
                    "ohos.permission.WRITE_MEDIA");
        }
    
        @PermissionGrant(REQUECT_CODE_SDCARD)
        public void requestSdcardSuccess() {
            new ToastDialog(this).setText("GRANT ACCESS SDCARD!").setDuration(0).show();
        }
    
        @PermissionDenied(REQUECT_CODE_SDCARD)
        public void requestSdcardFailed() {
            new ToastDialog(this).setText("DENY ACCESS SDCARD!").setDuration(0).show();
        }
    
        @PermissionGrant(REQUECT_CODE_READ_HEALTH_DATA)
        public void requestCallPhoneSuccess() {
            new ToastDialog(this).setText("GRANT ACCESS READHEALTHDATA!").setDuration(0).show();
        }
    
        @PermissionDenied(REQUECT_CODE_READ_HEALTH_DATA)
        public void requestCallPhoneFailed() {
            new ToastDialog(this).setText("DENY ACCESS READHEALTHDATA!").setDuration(0).show();
        }
    
        @PermissionGrant(REQUECT_CODE_MICROPHONE)
        public void requestContactSuccess() {
            new ToastDialog(this).setText("GRANT ACCESS MICROPHONE!").setDuration(0).show();
        }
    
        @PermissionDenied(REQUECT_CODE_MICROPHONE)
        public void requestContactFailed() {
            new ToastDialog(this).setText("DENY ACCESS MICROPHONE!").setDuration(0).show();
        }
}

```

* in RequestCode:

```java
public class RequestCode {
    public static final int REQUECT_CODE_SDCARD = 2;
    public static final int REQUECT_CODE_READ_HEALTH_DATA = 3;
    public static final int REQUECT_CODE_MICROPHONE = 4;
}
```

License
--------
Apache License Version 2.0, January 2004 http://www.apache.org/licenses
https://gitee.com/ts_ohos/mpermissions/blob/master/LICENSE
