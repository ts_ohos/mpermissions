package com.zhy.permission_sample;

import com.zhy.m.permission.MPermissions;
import com.zhy.m.permission.PermissionDenied;
import com.zhy.m.permission.PermissionGrant;
import com.zhy.m.permission.ShowRequestPermissionRationale;
import com.zhy.permission_sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;
import static com.zhy.permission_sample.RequestCode.*;

public class MainAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode,
                                                   String[] permissions,
                                                   int[] grantResults) {
        MPermissions.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
    }

    @ShowRequestPermissionRationale(REQUECT_CODE_SDCARD)
    public void whyNeedScCard() {
        new ToastDialog(this).setText("I need write news to sdcard!").setDuration(0).show();
        MPermissions.requestPermissions(
                MainAbility.this,
                REQUECT_CODE_SDCARD,
                "ohos.permission.WRITE_MEDIA");
    }

    @PermissionGrant(REQUECT_CODE_SDCARD)
    public void requestSdcardSuccess() {
        new ToastDialog(this).setText("GRANT ACCESS SDCARD!").setDuration(0).show();
    }

    @PermissionDenied(REQUECT_CODE_SDCARD)
    public void requestSdcardFailed() {
        new ToastDialog(this).setText("DENY ACCESS SDCARD!").setDuration(0).show();
    }

    @PermissionGrant(REQUECT_CODE_READ_HEALTH_DATA)
    public void requestCallPhoneSuccess() {
        new ToastDialog(this).setText("GRANT ACCESS HEALTHDATA!").setDuration(0).show();
    }

    @PermissionDenied(REQUECT_CODE_READ_HEALTH_DATA)
    public void requestCallPhoneFailed() {
        new ToastDialog(this).setText("DENY ACCESS HEALTHDATA!").setDuration(0).show();
    }

    @PermissionGrant(REQUECT_CODE_MICROPHONE)
    public void requestContactSuccess() {
        new ToastDialog(this).setText("GRANT ACCESS MICROPHONE!").setDuration(0).show();
    }

    @PermissionDenied(REQUECT_CODE_MICROPHONE)
    public void requestContactFailed() {
        new ToastDialog(this).setText("DENY ACCESS MICROPHONE!").setDuration(0).show();
    }
}
