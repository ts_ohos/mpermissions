package com.zhy.permission_sample.slice;

import com.zhy.m.permission.MPermissions;
import com.zhy.m.permission.ShowRequestPermissionRationale;
import com.zhy.permission_sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import static com.zhy.permission_sample.RequestCode.*;

public class MainAbilitySlice extends AbilitySlice {

    private Button mBtnSdcard, mBtnReadHealthData, mBtnMicrophone;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mBtnSdcard = (Button) findComponentById(ResourceTable.Id_id_btn_sdcard);
        mBtnReadHealthData = (Button) findComponentById(ResourceTable.Id_id_btn_readHealthData);
        mBtnMicrophone = (Button) findComponentById(ResourceTable.Id_id_btn_microphone);

        mBtnSdcard.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!MPermissions.shouldShowRequestPermissionRationale(
                        MainAbilitySlice.this,
                        "ohos.permission.WRITE_MEDIA", REQUECT_CODE_SDCARD)) {
                    MPermissions.requestPermissions(
                            MainAbilitySlice.this,
                            REQUECT_CODE_SDCARD,
                            "ohos.permission.WRITE_MEDIA");
                }
            }
        });

        mBtnReadHealthData.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                MPermissions.requestPermissions(MainAbilitySlice.this,
                        REQUECT_CODE_READ_HEALTH_DATA, "ohos.permission.READ_HEALTH_DATA");
            }
        });

        mBtnMicrophone.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                MPermissions.requestPermissions(
                        MainAbilitySlice.this,
                        REQUECT_CODE_MICROPHONE,
                        "ohos.permission.MICROPHONE");
            }
        });
    }

    @ShowRequestPermissionRationale(REQUECT_CODE_SDCARD)
    public void whyNeedScCard() {
        new ToastDialog(this).setText("I need write news to sdcard!").setDuration(0).show();
        MPermissions.requestPermissions(
                MainAbilitySlice.this, REQUECT_CODE_SDCARD, "ohos.permission.WRITE_MEDIA");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
