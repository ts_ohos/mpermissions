package com.zhy.permission_sample;

import com.zhy.m.permission.MPermissions;
import com.zhy.m.permission.Utils;
import com.zhy.permission_sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.zhy.permission_sample", actualBundleName);
    }

    @Test
    public void testNotNeedShowRationale() {
        AbilitySlice abilitySlice = new MainAbilitySlice();
        boolean isShow = MPermissions.shouldShowRequestPermissionRationale(abilitySlice, "ohos.permission.LOCATION", 1);
        Assert.assertFalse(isShow);
    }

    @Test
    public void testCompilerFailed() {
        AbilitySlice abilitySlice = new AbilitySlice();
        boolean throwError = false;
        try {
            MPermissions.requestPermissions(abilitySlice, 1001, "ohos.permission.LOCATION");
        }catch (Exception e) {
            throwError = true;
        }
        Assert.assertTrue(throwError);
    }

    @Test
    public void testDeniedPermissions() {
        Ability ability = new Ability();
        List<String> deniedPermissions = Utils.findDeniedPermissions(ability, "ohos.permission.LOCATION");
        Assert.assertEquals(deniedPermissions.get(0),"ohos.permission.LOCATION");
    }
}