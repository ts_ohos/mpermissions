package com.zhy.m.permission;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.bundle.IBundleManager;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by namee on 2015. 11. 18..
 */
final public class Utils {
  private Utils(){}

  public static List<String> findDeniedPermissions(Ability ability, String... permission) {
    List<String> denyPermissions = new ArrayList<>();
    for (String value : permission) {
      if (ability.verifySelfPermission(value) != IBundleManager.PERMISSION_GRANTED) {
        denyPermissions.add(value);
      }
    }
    return denyPermissions;
  }

  public static List<Method> findAnnotationMethods(
          Class clazz, Class<? extends Annotation> clazz1) {
    List<Method> methods = new ArrayList<>();
    for (Method method : clazz.getDeclaredMethods()) {
      if (method.isAnnotationPresent(clazz1)) {
        methods.add(method);
      }
    }
    return methods;
  }



  public static Ability getAbility(Object object) {
    if (object instanceof AbilitySlice) {
      return ((AbilitySlice) object).getAbility();
    } else if (object instanceof Ability) {
      return (Ability) object;
    }
    return null;
  }
}
